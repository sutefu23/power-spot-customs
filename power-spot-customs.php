<?php
/*
Plugin Name: パワースポットサイトカスタマイズ
Plugin URI:
Description: パワースポットサイトのカスタム用です。フロント固定ページにカテゴリ一覧とサイドバーに相互リンクを自動で付けます。
Version: 1.0
Author: HK
Author URI:
License: GPL2
*/


/**
 * エリア別、ご利益別のカテゴリリストを出力
 */
function insert_category_list_on_top($content){
	if(is_front_page()){

		$parent_names = array('エリア別','ご利益別');

		$category_list_html .= '<div class="top_category_list">';
		foreach ($parent_names as $i => $parent_name) {
			$cat_ID = get_cat_ID($parent_name);
			$cats = get_categories(
	    		array( 'parent' => $cat_ID )
			);
			$class = ($i==0)?"fa-map-marker":"fa-tag";
			$category_list_html .= '<div class="top_category_list_item">';
			$category_list_html .= '<h4><i class="fa '. $class .'"></i>'. $parent_name .'</h4>';
			$category_list_html .= '<ul>';

			foreach ( $cats as $cat ) {
				$list_class = ($i==0)?"fa-arrow-circle-o-right":"fa-arrow-circle-right";
		        $category_list_html .=  '<li><i class="fa '.$list_class.'" aria-hidden="true"></i><a href="'. get_category_link( $cat->term_id ) . '">' . $cat->name . '</a></li>';
			}
			$category_list_html .= '</ul>';
			$category_list_html .= '</div>';

		}
		$category_list_html .= '</div>';

		$content = $category_list_html . $content;
	}
	return $content;
}
add_filter( 'the_content', 'insert_category_list_on_top' ,10);


function customize_category_list( $args ) {
    $args['hierarchical'] = true;
    return $args;
}
add_filter( 'widget_categories_args', 'customize_category_list' );

/**
 * フロントページのインラインＣＳＳ
 */
function psc_front_page_inline_css(){
	if(is_front_page()){
	?>
	<style type="text/css" media="screen">
		#breadcrumb{
			display: none;
		}
		.top_category_list{
			display: flex;
		    justify-content: space-between;				
		}
		.top_category_list_item{
			width: 49%;
		}
		.top_category_list_item ul{
			box-sizing: border-box;
			list-style: none;
		    padding-left: 1.5em;
		}
		.top_category_list .fa{
			margin-right: 0.3em;
		}

	@media screen and (max-width: 320px) {
		.top_category_list_item ul{
		    padding-left: 1.0em;
		    padding-right: 1.0em;
			font-size: 0.9em;
		}
	}
	</style>
	<?php
	}
}
add_filter( 'wp_head', 'psc_front_page_inline_css' );

function psc_post_inline_css(){
	?>
	<style type="text/css" media="screen">
	.heading-img {
	    position: relative;
	    margin-bottom: 3rem;
	    text-align: center;
	}
	.heading-img img {
	    width: 100%;
	    max-width: 640px;
	}
	.heading-txt {
	    max-width: 640px;
	    width: 100%;
	    height: 100%;
	    background: rgba(255, 255, 255, 0.6);
	    position: absolute;
	    top: 0;
	    bottom: 0;
	    left: 0;
	    right: 0;
	    margin: auto;
	}
	.heading-img span {
	    width: 90%;
	    text-align: center;
	    font-size: 120%;
	    font-weight: bold;
	    color: <?php echo get_theme_mod('main_color', '#d34c4c'); ?>;
	    border-top: solid 1px <?php echo get_theme_mod('main_color', '#d34c4c'); ?>;
	    border-bottom: solid 1px<?php echo get_theme_mod('main_color', '#d34c4c'); ?>;
	    padding: 25px 0;
		position: absolute;
	    top: calc(50% - 1em);
	    left: 0;
	    right: 0;
	    margin: auto;
	    display: block;
	}	
	</style>
	<?php
}
add_filter( 'wp_head', 'psc_post_inline_css' );


/**
 * インラインJS
 */
function psc_inline_scripts() {
   	if ( ! wp_script_is( 'jquery', 'done' ) ) {
    	wp_enqueue_script( 'jquery' );
   	}	
?>
<script async>
	jQuery(function($) {
		$(function () {
			$.getJSON('https://script.google.com/macros/s/AKfycbwwwdieKlgyXdxAwq8hF767qTM0QKOp3HakfRjx3yeasK8jFDs/exec',function(data){
				var html = '<div id="site-links" class="widget">';
				html += '<h4 class="widgettitle fa-pencil">' + data[0]['title'] + '</h4>';
				html += '<ul>';
				for(var i = 0 ; i < data.length; i++){
					if(data[i]['url'].indexOf(document.domain) >= 0) continue;
					if(!data[i]['button_image']){
						html += '<li><a href="' + data[i]['url'] + '">' + data[i]['text'] + '</a>';
					}else{
						html += '<li><a href="' + data[i]['url'] + '"><img src="' + data[i]['button_image'] + '"></a>';
					}
				}
				html += '</ul>';
				html += '</div>';
				$('aside .widget:last-child').after(html)

			});
		})
	});
</script>
<?php 
if(get_option('power_spot_title_images')&&is_single()){
	$image_ids = get_option('power_spot_title_images');
 ?>
<script async>
jQuery(function($) {
	var img_srcs = new Array();
	<?php foreach ($image_ids as $image_id) {
		echo 'img_srcs.push(\'' .wp_get_attachment_image_src($image_id,'full',false)[0] .'\');';
	}
	 ?>
  var len = $('.entry-content h2').length;
    $('h2').each(function(i){
		index = i % img_srcs.length;
	    $(this).after('<div class="heading-img"><img src="'+ img_srcs[index] +'"><div class="heading-txt"><span>' + $(this).text() + '</span></div></div>');
    });
  });

</script>
<?php
	}//end if get_option('power_spot_title_images')
?>

<?php
}
add_action( 'wp_footer', 'psc_inline_scripts' );


function my_enqueue($hook) {
    if ( $hook != 'toplevel_page_title-image' ) {
        return;
    }
	wp_enqueue_media();
    wp_enqueue_script(
        'my-media-uploader',
        plugins_url("js/media-uploader.js", __FILE__),
        array('jquery'),
        filemtime(dirname(__FILE__).'/media-uploader.js'),
        false
    );
   	if ( ! wp_script_is( 'jquery-ui', 'done' ) ) {
    	wp_enqueue_script( 'jquery-ui' );
   	}	
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );

// 設定画面追加
add_action( 'admin_menu', 'add_plugin_admin_menu' );

// アクションフックで呼ばれる関数
function add_plugin_admin_menu() {
    add_menu_page(
          'タイトル画像',
          'タイトル画像設定',
          'administrator', // capability
          'title-image',
          'set_title_image', // function
          'dashicons-format-image', // icon_url
          50 // position
     );
}
// 設定画面用のHTML
function set_title_image() {
?>
<style type="text/css">
#upload-images img
{
	cursor:move;
    max-width: 200px;
    max-height: 200px;
    margin: 10px;
    border: 1px solid #cccccc;
}
.submit-field{
	text-align: right;
	margin-top: 20px;
	margin-right: 50px;
}
.image-remove{
	cursor: pointer;
	color: red;
	margin-top: -0.5em;
	display: block;
}
</style>
<?php 
if(wp_script_is('jquery','done')):
?>
<script>
jQuery(document).ready(function($){
	$('.image-remove').on('click',function(e){
		var image_id = $(this).attr('image-id');
		$('[image-id='+ image_id +']').remove();
	});

    $('#save-media').on('click',function(e) {
        var image_ids = new Array();
        $('#upload-images img').each(function(i){
            image_ids[i] = $(this).attr('image-id');
        })
        var data = {
            action: 'title_image_save',
            image_ids: encodeURIComponent(JSON.stringify(image_ids)),
        };
        $.post('admin-ajax.php', data, function(response) {
			if (response.indexOf('true') > -1 || response == true) {
			    location.reload();
			} else {
			    alert("保存出来ませんでした。\n" + response);
			    $('#save-media').data("valid", false);
			}
        });
        return false;
     });
});
</script>
<script>
jQuery(function($) {
	$(function () {
		$( "#upload-images" ).sortable();
    	$( "#upload-images" ).disableSelection();
	})
});

</script>
<?php
endif;
 ?>
	<div class="wrap">
	<h2>投稿タイトル用画像選択</h2>
	<p>ここで選択されたものが順番に投稿のh2の背景画像として設定されます。連続で選択が可能です。<br>
	画像をドラッグすると並べ替えを行います。</p>
	<button id="select-media">画像を追加</button>
	<div id="upload-images">
		<?php 
		if(get_option('power_spot_title_images')){
			$image_ids = get_option('power_spot_title_images');
			foreach ($image_ids as $image_id) {
				echo '<div class="image-item">';
				echo '<img image-id="'. $image_id .'" src="'.wp_get_attachment_image_src($image_id,'full',false)[0] .'" />';
				echo '<span class="image-remove" image-id="'. $image_id .'">削除</span>';
				echo '</div>';

			}
		} ?>
	</div>
	<div class="submit-field">		
	<button id="save-media" class="button button-primary button-large">保存</button>
	</div>
	</div><!-- .wrap -->
<?php
}



function title_image_save() {
	$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
	$domain = home_url();
	if( strpos($referer, $domain) === false ){
		die();
	}
	$image_ids = json_decode( rawurldecode($_POST['image_ids']),true);
	update_option('power_spot_title_images',$image_ids);
	echo 'true';
}
add_action('wp_ajax_title_image_save', 'title_image_save');